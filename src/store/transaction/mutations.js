export const appendTransactions = (state, payload) => {
  for (var i = 0; i < payload.length; i++) {
    var data = payload[i].data()
    state.Transactions.push({
      Raw: payload[i],
      ID: payload[i].id,
      AccountID: data.AccountID,
      Description: data.Description,
      Amount: data.Amount,
      CategoryID: data.Category,
      Date: data.Date,
      Debit: data.Debit,
      Transfer: data.Transfer,
      Tags: !data.Tags ? [] : Object.keys(data.Tags)
    })
  }
}

export const setHasMoreData = (state, payload) => {
  state.HasMoreData = payload
}

export const setLoading = (state, payload) => {
  state.Loading = payload
}

export const setPrimed = state => {
  state.Primed = true
}

export const clearTransactions = (state, payload) => {
  state.Transactions = []
}

export const transactionDeleted = (state, ID) => {
  state.Transactions = state.Transactions.filter(el => el.ID !== ID)
}

export const setFilterValue = (state, payload) => {
  var filter = payload.Filter

  filter.Value = (payload.Value === null ? filter.DefaultValue : payload.Value)
}

export const clearFilterValue = (state, payload) => {
  var filter = state.Filters.filter(el => el.Key === payload.Key)[0]

  if (filter) { filter.Value = filter.DefaultValue }
}
