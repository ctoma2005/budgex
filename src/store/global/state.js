import currencies from '../../data/currencies.json'
import crypto from '../../data/crypto.json'
import categories from '../../data/categories.json'
import accounts from '../../data/accounts.json'

export const filterTypes = {
  DATE: 1,
  SELECT: 2,
  FLAG: 3
}

export default {
  loading: false,
  error: null,
  success: null,
  primed: false,
  slidingRight: false,
  currencies: currencies.concat(crypto),
  categories: categories,
  accounts: accounts
}

export const dateRangeTypes = {
  LAST1DAY: 1,
  LAST3DAYS: 2,
  LASTWEEK: 3,
  LAST2WEEKS: 4,
  LASTMONTH: 5,
  CUSTOM: 6
}

export const dateViewTypes = {
  MONTH: 'D',
  YEAR: 'D MMM',
  DECADE: 'YYYY-MM-DD'
}

export const dateRangeOptions = [
  {label: 'Last day', value: dateRangeTypes.LAST1DAY},
  {label: 'Last 3 days', value: dateRangeTypes.LAST3DAYS},
  {label: 'Last week', value: dateRangeTypes.LASTWEEK},
  {label: 'Last 2 weeks', value: dateRangeTypes.LAST2WEEKS},
  {label: 'Last month', value: dateRangeTypes.LASTMONTH},
  {label: 'Custom', value: dateRangeTypes.CUSTOM}
]
