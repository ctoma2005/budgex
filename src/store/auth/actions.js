export const autoLogin = function (store, fb) {
  return new Promise((resolve, reject) => {
    var unsubscribe = fb.auth().onAuthStateChanged(function (user) {
      unsubscribe()
      store.commit('setUser', user)

      resolve(store.getters.authenticated)
    }, err => {
      unsubscribe()
      reject(err)
    })
  })
}

export const login = async function (store, {email, password, fb}) {
  let user = await fb.auth().signInWithEmailAndPassword(email, password)
  store.commit('setUser', user)
  return user
}

export const logOut = async function (store, {fb}) {
  store.state.user = null
  await fb.auth().signOut()
}

export const register = async function (store, {email, password, fb, seedCategories, seedAccounts}) {
  let user = await fb.auth().createUserWithEmailAndPassword(email, password)
  store.commit('setUser', user)

  await store.dispatch('seedUser', {fb, seedCategories, seedAccounts})
  return user
}

export const seedUser = async function (store, {fb, seedCategories, seedAccounts}) {
  var db = fb.firestore()
  var uid = store.state.user.uid

  // Get a new write batch
  var batch = db.batch()

  var portfolioRef = db.collection('portfolios').doc(uid)
  batch.set(portfolioRef, {name: 'My portfolio'})

  if (seedCategories) {
    var categoriesRef = portfolioRef.collection('categories')

    addCategories(store, batch, categoriesRef)
  }

  if (seedAccounts) {
    var accountsRef = portfolioRef.collection('accounts')

    addAccounts(store, batch, accountsRef)
  }

  // Commit the batch
  await batch.commit()
}

export const addCategories = function (store, batch, categoriesRef) {
  var categories = store.rootState.global.categories

  for (var i = 0; i < categories.length; i++) {
    var cat = categories[i]

    batch.set(categoriesRef.doc(), cat)
  }
}

export const addAccounts = function (store, batch, accountsRef) {
  var accounts = store.rootState.global.accounts

  for (var i = 0; i < accounts.length; i++) {
    var cat = accounts[i]

    batch.set(accountsRef.doc(), cat)
  }
}
