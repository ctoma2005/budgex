import {v1} from 'uuid'
import {filterKeys} from './state'
import {date} from 'quasar'
const {formatDate, isSameDate} = date

export const refresh = async function (store, payload) {
  store.commit('clearTransactions')

  await store.dispatch('runQuery', payload)
}

export const runQuery = async function (store, payload) {
  store.commit('setLoading', true)
  store.commit('setHasMoreData', null)

  try {
    var db = payload.fb.firestore()
    var uid = store.rootGetters['auth/uid']
    var ref = db.collection('portfolios').doc(uid).collection('ledger')
      .orderBy('Date', 'desc')
      .orderBy('CreatedAt', 'desc')
      .limit(store.state.PageSize)

    ref = store
      .rootGetters['global/enabledFilters'](store.state.Filters)
      .reduce((acc, currentValue) => applyFilter(store, acc, currentValue), ref)

    if (!store.getters.transactionsEmpty) {
      ref = ref.startAfter(store.state.Transactions[store.state.Transactions.length - 1].Raw)
    }

    var transactionsSnapshot = await ref.get()

    store.commit('appendTransactions', transactionsSnapshot.docs)

    store.commit('setHasMoreData', transactionsSnapshot.docs.length >= store.state.PageSize)
  } catch (err) {
    throw err
  } finally {
    store.commit('setLoading', false)

    if (!store.state.Primed) {
      store.commit('setPrimed')
    }
  }
}

export const deleteTransaction = async function (store, payload) {
  var db = payload.fb.firestore()
  var uid = store.rootGetters['auth/uid']
  var transaction = payload.transaction

  var accountsRef = db
    .collection('portfolios')
    .doc(uid)
    .collection('accounts')

  var ledgerRef = db
    .collection('portfolios')
    .doc(uid)
    .collection('ledger')

  var acc = store.rootGetters['account/accountWithID'](transaction.AccountID)

  var balance = await db.runTransaction(async tr => {
    var newBalance = -1

    if (acc) {
      var account = await tr.get(accountsRef.doc(transaction.AccountID))
      newBalance = account.data().Balance + (transaction.Debit ? transaction.Amount : -transaction.Amount)

      tr.update(account.ref, {
        Balance: newBalance
      })
    }

    tr.delete(ledgerRef.doc(transaction.ID))

    return newBalance
  })

  if (acc) {
    store.commit('account/accountEdited', {ID: transaction.AccountID, Props: {Balance: balance}}, {root: true})
    store.commit('global/setSuccess', `${acc.Name} ${(transaction.Debit ? 'credited' : 'debited')} with ${transaction.Amount} ${acc.Currency.Symbol}`, {root: true})
  } else {
    store.commit('global/setSuccess', 'Transaction deleted!', {root: true})
  }
  store.commit('transactionDeleted', transaction.ID)
}

export const addTransaction = async function (store, payload) {
  var db = payload.fb.firestore()
  const timestamp = payload.fb.firestore.FieldValue.serverTimestamp()

  var uid = store.rootGetters['auth/uid']

  var debit = payload.Debit

  var accountsRef = db
    .collection('portfolios')
    .doc(uid)
    .collection('accounts')

  var ledgerRef = db
    .collection('portfolios')
    .doc(uid)
    .collection('ledger')

  var accID = payload.Account.ID
  var [balance, transactionsSum] = await db.runTransaction(async transaction => {
    var account = await transaction.get(accountsRef.doc(accID))

    for (var i = 0; i < payload.Transactions.length; i++) {
      var currentTr = payload.Transactions[i]

      var tr = {
        CreatedAt: timestamp,
        Date: payload.Date,
        Amount: currentTr.Amount,
        AccountID: accID,
        Debit: debit
      }

      if (currentTr.Category) {
        tr.Category = currentTr.Category
        var tags = arrayToFlags(currentTr.Tags)

        if (tags) { tr.Tags = tags }
      }

      if (currentTr.Description) {
        tr.Description = currentTr.Description
      }

      transaction.set(ledgerRef.doc(), tr)
    }

    var transactionsSum = payload.Transactions
      .map(t => t.Amount)
      .reduce((a, b) => a + b)

    var balance = account.data().Balance + (debit ? -transactionsSum : transactionsSum)

    transaction.update(account.ref, {
      Balance: balance
    })

    return [balance, transactionsSum]
  })

  store.commit('account/accountEdited', {ID: accID, Props: {Balance: balance}}, {root: true})
  store.commit('global/setSuccess', `${payload.Account.Name} ${(debit ? 'debited' : 'credited')} with ${transactionsSum} ${payload.Account.Currency.Symbol}`, {root: true})

  store.dispatch('refresh', {fb: payload.fb})
}

export const editTransaction = async function (store, payload) {
  var db = payload.fb.firestore()
  const timestamp = payload.fb.firestore.FieldValue.serverTimestamp()

  var uid = store.rootGetters['auth/uid']

  var debit = payload.Debit

  var accountsRef = db
    .collection('portfolios')
    .doc(uid)
    .collection('accounts')

  var ledgerRef = db
    .collection('portfolios')
    .doc(uid)
    .collection('ledger')

  var accID = payload.Account.ID

  var [oldBalance, balance] = await db.runTransaction(async transaction => {
    var tr = await transaction.get(ledgerRef.doc(payload.TransactionID))
    var account

    var lastAmount = tr.data().Amount
    var diff = payload.Amount - lastAmount

    if (diff !== 0) { account = await transaction.get(accountsRef.doc(accID)) }

    transaction.update(tr.ref, {
      CreatedAt: timestamp,
      Amount: payload.Amount,
      Category: payload.Category || payload.fb.firestore.FieldValue.delete(),
      Tags: arrayToFlags(payload.Tags) || payload.fb.firestore.FieldValue.delete(),
      Description: payload.Description || payload.fb.firestore.FieldValue.delete(),
      Date: payload.Date
    })

    var oldBalance = 0
    var balance = 0

    if (diff !== 0) {
      balance = account.data().Balance
      oldBalance = balance
      balance += (debit ? -diff : diff)

      transaction.update(account.ref, {
        Balance: balance
      })
    }

    return [oldBalance, balance]
  })

  if (oldBalance !== balance) {
    store.commit('account/accountEdited', {ID: accID, Props: {Balance: balance}}, {root: true})
    store.commit('global/setSuccess', `${payload.Account.Name} ${(oldBalance < balance ? 'credited' : 'debited')} with ${Math.abs(balance - oldBalance)} ${payload.Account.Currency.Symbol}`, {root: true})
  }

  store.dispatch('refresh', {fb: payload.fb})
}

export const addTransfer = async function (store, payload) {
  var db = payload.fb.firestore()

  const timestamp = payload.fb.firestore.FieldValue.serverTimestamp()

  var uid = store.rootGetters['auth/uid']

  var accountsRef = db
    .collection('portfolios')
    .doc(uid)
    .collection('accounts')

  var ledgerRef = db
    .collection('portfolios')
    .doc(uid)
    .collection('ledger')

  var source = payload.SourceAccount
  var destination = payload.DestinationAccount

  var {sourceBalance, destinationBalance} = await db.runTransaction(async transaction => {
    var sourceAccount = await transaction.get(accountsRef.doc(source.ID))
    var destinationAccount = await transaction.get(accountsRef.doc(destination.ID))
    var groupID = v1()

    var sourceTr = {
      CreatedAt: timestamp,
      Date: payload.Date,
      Amount: source.Amount,
      AccountID: source.ID,
      Debit: true,
      Transfer: true,
      Split: false,
      GroupID: groupID
    }

    var destinationTr = {
      CreatedAt: timestamp,
      Date: payload.Date,
      Amount: destination.Amount,
      AccountID: destination.ID,
      Debit: false,
      Transfer: true,
      Split: false,
      GroupID: groupID
    }

    if (payload.Description) {
      sourceTr.Description = payload.Description
      destinationTr.Description = payload.Description
    }

    transaction.set(ledgerRef.doc(), sourceTr)
    transaction.set(ledgerRef.doc(), destinationTr)

    var sourceBalance = sourceAccount.data().Balance - source.Amount
    var destinationBalance = destinationAccount.data().Balance + destination.Amount

    transaction.update(sourceAccount.ref, {
      Balance: sourceBalance
    })

    transaction.update(destinationAccount.ref, {
      Balance: destinationBalance
    })

    return {sourceBalance, destinationBalance}
  })

  store.commit('account/accountEdited', {ID: source.ID, Props: {Balance: sourceBalance}}, {root: true})
  store.commit('account/accountEdited', {ID: destination.ID, Props: {Balance: destinationBalance}}, {root: true})
  store.commit('global/setSuccess', `Transfered ${source.Amount} ${source.Currency.Symbol} from ${source.Name}`, {root: true})

  store.dispatch('refresh', {fb: payload.fb})
}

export const setFilterValue = async function (store, payload) {
  var filter = store.getters.filterByKey(payload.Key)
  if (!filter) return

  var oldValue = filter.Value

  store.commit('setFilterValue', {Filter: filter, Value: payload.Value})

  if (!store.rootGetters['global/filterChanged'](filter.Type, filter.Value, oldValue)) { return }

  await store.dispatch('global/doAction', {
    route: 'transaction/refresh',
    args: {
      fb: this._vm.$fb
    },
    showLoading: true,
    errorMessage: 'Failed to query transactions'
  }, {root: true})
}

export const clearFilterValue = async function (store, payload) {
  if (store.rootGetters['global/filterChanged'](payload)) {
    store.commit('clearFilterValue', payload)
  }
}

export const applyFilter = function (store, query, filter) {
  switch (filter.Key) {
    case filterKeys.ACCOUNT:
      return query.where('AccountID', '==', filter.Value)
    case filterKeys.DATE:
      var range = store.rootGetters['global/dateFromRange'](filter.Value)
      var from = range.From || new Date()
      var to = range.To || new Date()

      return query
        .where('Date', '>=', formatDate(from, 'YYYY-MM-DD'))
        .where('Date', '<=', formatDate(to, 'YYYY-MM-DD') + (isSameDate(from, to, 'day') ? '~' : ''))
    case filterKeys.TYPE:
      return query
        .where('Debit', '==', filter.Value)
    case filterKeys.CATEGORY:
      return query
        .where('Category', '==', filter.Value)
    default:
      return query
  }
}

const arrayToFlags = function (arr) {
  if (!arr || arr.length === 0) { return null }

  var res = {}
  for (var i = 0; i < arr.length; i++) {
    res[arr[i]] = true
  }

  return res
}
