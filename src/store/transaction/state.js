import {filterTypes, dateRangeTypes} from '../global/state'

export const filterKeys = {
  DATE: 1,
  ACCOUNT: 2,
  TYPE: 3,
  CATEGORY: 4
}

export default {
  Primed: false,
  Loading: false,
  HasMoreData: null,
  Transactions: [],
  PageSize: 20,
  Filters: [
    {
      Key: filterKeys.TYPE,
      Type: filterTypes.FLAG,
      Action: 'transaction/setFilterValue',
      LabelTrue: 'Expense',
      LabelFalse: 'Income',
      LabelIndeterminate: 'All',
      Value: null,
      DefaultValue: null,
      HideClear: true
    },
    {
      Key: filterKeys.CATEGORY,
      Type: filterTypes.SELECT,
      Icon: 'list',
      Label: 'Category',
      OptionsGetter: 'transaction/optionsFilterCategories',
      ValueGetter: 'transaction/filterValue',
      Action: 'transaction/setFilterValue',
      Value: '',
      DefaultValue: ''
    },
    {
      Key: filterKeys.ACCOUNT,
      Type: filterTypes.SELECT,
      Icon: 'account_box',
      Label: 'Account',
      OptionsGetter: 'account/optionsAccounts',
      ValueGetter: 'transaction/filterValue',
      Action: 'transaction/setFilterValue',
      Value: '',
      DefaultValue: ''
    },
    {
      Key: filterKeys.DATE,
      Type: filterTypes.DATE,
      Icon: 'today',
      Label: 'Period',
      OptionsGetter: 'global/commonDateRanges',
      ValueGetter: 'transaction/filterValue',
      Action: 'transaction/setFilterValue',
      Value: {
        Range: dateRangeTypes.LASTWEEK,
        From: null,
        To: null
      },
      DefaultValue: {
        Range: dateRangeTypes.LASTWEEK,
        From: null,
        To: null
      }
    }
  ]
}
