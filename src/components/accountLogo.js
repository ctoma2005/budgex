// import {mapGetters} from 'vuex'
import {QChip} from 'quasar'

export default {
  props: {
    account: {
      type: Object,
      required: true
    },
    inline: {
      type: Boolean,
      default: false
    },
    color: {
      type: String,
      default: 'secondary'
    }
  },
  computed: {
    acronym () {
      return this.$store.getters['account/acronym'](this.account)
    }
  },
  render (h) {
    var acr = h('strong', [this.acronym])

    return this.inline
      ? acr
      : h(QChip, {props: {color: this.color}}, [acr])
  }
}
