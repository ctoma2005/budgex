import Vue from 'vue'
import VueRouter from 'vue-router'

import routes from './routes'
import store from '../store'
import {Loading, Notify} from 'quasar'

Vue.use(VueRouter)

const Router = new VueRouter({
  /*
   * NOTE! Change Vue Router mode from quasar.conf.js -> build.env.VUE_ROUTER_MODE
   *
   * If you decide to go with "history" mode, please also set "build.publicPath"
   * to something other than an empty string.
   * Example: '/' instead of ''
   */

  // Leave as is and change from quasar.conf.js instead!
  mode: process.env.VUE_ROUTER_MODE,
  base: process.env.VUE_ROUTER_BASE,
  scrollBehavior: () => ({ y: 0 }),
  routes
})

Router.autoLogin = true

Router.beforeEach(async (to, from, next) => {
  var middleware = [handleAutoLogin, auth, prime]

  var handled = false

  for (var i = 0; i < middleware.length; i++) {
    handled = await middleware[i](to, from, next)

    if (handled) { break }
  }

  Loading.hide()

  if (!handled) { next() }
})

const prime = async (to, from, next) => {
  if (!(/^\/portfolio/.test(to.path)) || store.state.global.primed) {
    return false
  }

  try {
    Loading.show()

    await store.dispatch('global/prime', store._vm, {root: true})
  } catch (_) {
    return false
  }
}

const handleAutoLogin = async (to, from, next) => {
  if (!Router.autoLogin) {
    return false
  }

  Router.autoLogin = false

  if (/^\/auth/.test(to.path)) {
    return false
  }

  try {
    var authenticated = await store.dispatch('auth/autoLogin', Router.app.$fb)

    if (!authenticated) {
      next(`/auth/login?returnUrl=${to.path}`)
      return true
    }
  } catch (err) {
    if (err) {
      Notify.create(`Auto login failed with: ${err.message}`)
      next('/auth/login')
      return true
    }
  }

  return false
}

const auth = async (to, from, next) => {
  if (/^\/auth/.test(to.path)) {
    return false
  }

  if (!store.getters['auth/authenticated']) {
    next(`/auth/login?returnUrl=${to.path}`)
    return true
  }

  return false
}

export default Router
