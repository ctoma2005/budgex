import { Notify } from 'quasar'

export default {
  methods: {
    addTransaction (acc, accountsCount) {
      if (accountsCount === 0) {
        Notify.create('You have no accounts. Consider adding one')
        return
      }
      this.$q.actionSheet({
        title: `Add transaction ${acc ? 'to ' + acc.Name : ''}`,
        grid: false,
        actions: [
          {
            label: 'Expense',
            icon: 'remove_circle',
            color: 'negative'
          },
          {
            label: 'Income',
            icon: 'add_circle',
            color: 'positive'
          },
          {
            label: 'Transfer',
            icon: 'swap_horiz',
            color: 'secondary'
          }
        ]
      })
        .then(response => {
          // TODO: find a way to disable the action
          if (response.label === 'Transfer' && accountsCount < 2) {
            Notify.create('Transfer requires at least 2 accounts!')
            return
          }

          var route = acc
            ? response.label === 'Transfer' ? `/portfolio/transfer/${acc.ID}` : `/portfolio/transaction/${acc.ID}/${response.label}`
            : `/portfolio/choseAccount/${response.label}`

          this.$router.push(route)
        })
        .catch(_ => { })
    }
  }
}
