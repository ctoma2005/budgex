import * as getters from './getters'
import * as mutations from './mutations'
import * as state from './state'
import * as actions from './actions'

export default{
  namespaced: true,
  getters,
  mutations,
  state: state.default,
  actions
}
