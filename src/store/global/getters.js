
import {filterTypes, dateRangeOptions, dateRangeTypes, dateViewTypes} from './state'
import {date} from 'quasar'
const {subtractFromDate, isValid, isSameDate, formatDate} = date

const today = Date()

export const currencyFromRaw = state => input => {
  return input => {
    var matches = /^(.+)\((.+)\)/.exec(input)

    if (matches === null || matches.length < 2) {
      return {
        Description: input,
        Symbol: ''
      }
    } else if (matches.length === 3) {
    // matched self
      return {
        Description: matches[1],
        Symbol: matches[2]
      }
    }

    return {
      Description: matches[0] || '',
      Symbol: matches[1] || ''
    }
  }
}

export const formatAmount = state => amount => {
  var parsed = Number.parseFloat(amount)
  if (isNaN(parsed)) {
    return 0
  }

  return +(parsed.toFixed(2))
}

export const currencyToRaw = state => input => {
  var s = input.Symbol ? `(${input.Symbol})` : ''
  return `${input.Description}${s}`
}

export const currencyList = state => {
  return state.currencies.map(c => {
    var v = `${c.Description} (${c.Symbol})`
    return {
      label: v,
      value: v
    }
  })
}

export const enabledFilters = (state, getters, rootState, rootGetters) => filters => {
  return filters.filter(f => isEnabledInternal(f, rootGetters))
}

export const enabledFiltersCount = (state, getters) => filters => {
  return getters.enabledFilters(filters).length
}

export const hasEnabledFilters = (state, getters) => filters => {
  return getters.enabledFiltersCount(filters) > 0
}

export const isFilterEnabled = (state, getters, rootState, rootGetters) => filter => {
  return isEnabledInternal(filter, rootGetters)
}

export const filterDescription = (state, getters, rootState, rootGetters) => (filter, condensed = false) => {
  if (!isEnabledInternal(filter, rootGetters)) return ''

  var res = condensed ? '' : filter.Label + ': '

  switch (filter.Type) {
    case filterTypes.SELECT:
      return res + rootGetters[filter.OptionsGetter].filter(o => o.value === filter.Value)[0].label
    case filterTypes.DATE:
      if (filter.Value.Range === dateRangeTypes.CUSTOM) {
        var rangeFormatted = dateRangeFormatted(state)(filter.Value.From, filter.Value.To, rootGetters)

        return res + rangeFormatted.Range
      }

      return res + rootGetters[filter.OptionsGetter].filter(o => o.value === filter.Value.Range)[0].label
    case filterTypes.FLAG:
      return filter.Value ? filter.LabelTrue : filter.LabelFalse
    default: return ''
  }
}

const isEnabledInternal = (filter, rootGetters) => {
  switch (filter.Type) {
    case filterTypes.SELECT:
      return filter.Value && rootGetters[filter.OptionsGetter].filter(o => o.value === filter.Value)[0]
    case filterTypes.DATE:
      return filter.Value && (filter.Value.Range !== dateRangeTypes.CUSTOM || (isValid(filter.Value.From) && isValid(filter.Value.To)))
    case filterTypes.FLAG:
      return filter.Value !== null
    default: return false
  }
}

export const isDefaultFilter = state => filter => {
  switch (filter.Type) {
    case filterTypes.SELECT:
    case filterTypes.FLAG:
      return filter.Value !== filter.DefaultValue
    case filterTypes.DATE:
      return filter.Value.Range !== filter.DefaultValue.Range
    default: return false
  }
}

export const filterChanged = state => (filterType, newValue, oldValue) => {
  switch (filterType) {
    case filterTypes.SELECT:
    case filterTypes.FLAG:
      return newValue !== oldValue
    case filterTypes.DATE:
      if (newValue.Range === dateRangeTypes.CUSTOM) { return newValue.From && newValue.To }

      return newValue.Range !== oldValue.Range
    default: return false
  }
}

export const commonDateRanges = state => dateRangeOptions

export const dateFromRange = state => filterValue => {
  switch (filterValue.Range) {
    case dateRangeTypes.LAST1DAY:
      return {From: subtractFromDate(new Date(), {days: 1}), To: null}
    case dateRangeTypes.LAST3DAYS:
      return {From: subtractFromDate(new Date(), {days: 3}), To: null}
    case dateRangeTypes.LASTWEEK:
      return {From: subtractFromDate(new Date(), {days: 7}), To: null}
    case dateRangeTypes.LAST2WEEKS:
      return {From: subtractFromDate(new Date(), {days: 14}), To: null}
    case dateRangeTypes.LASTMONTH:
      return {From: subtractFromDate(new Date(), {month: 1}), To: null}
    case dateRangeTypes.CUSTOM:
      return {From: filterValue.From, To: filterValue.To}
  }
}

export const todayFormat = state => date => {
  return isSameDate(date, today, 'day') ? 'Today' : ''
}

export const dateFormatted = (state) => (date, dateViewFormat = dateViewTypes.YEAR) => {
  return formatDate(date, dateViewFormat)
}

export const dateRangeFormatted = (state) => (from, to, rootGetters) => {
  var sameYear = (from && to) ? isSameDate(from, to, 'year') : true
  var sameMonth = (from && to) ? (sameYear ? isSameDate(from, to, 'month') : false) : true
  var dateFormat = sameYear ? (sameMonth ? dateViewTypes.MONTH : dateViewTypes.YEAR) : dateViewTypes.DECADE

  var f = from ? rootGetters['global/dateFormatted'](from, dateFormat) : ''
  var t = to ? rootGetters['global/dateFormatted'](to, dateFormat) : ''

  var month = from ? rootGetters['global/monthName'](from) : (to ? rootGetters['global/monthName'](to) : '')

  return {
    From: (sameMonth && f) ? `${f} ${month}` : (f || ' '),
    To: (sameMonth && t) ? `${t} ${month}` : (t || ' '),
    Range: (sameMonth
      ? `${f} - ${t} ${month}`
      : `${f} - ${t}`)
  }
}

export const monthName = state => date => {
  return formatDate(date, 'MMM')
}
