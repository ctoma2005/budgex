export default ({ Vue }) => {
  Vue.filter('numeral', function (value) {
    switch (value) {
      case 0:
        return ''
      case 1:
        return 'st'
      case 2:
        return 'nd'
      case 3:
        return 'rd'
      default:
        return 'th'
    }
  })
}
