export default [
  {
    path: '/',
    redirect: '/portfolio'
  },
  {
    path: '/portfolio',
    component: () => import('layouts/portfolio'),
    children: [
      {
        path: '',
        component: () => import('pages/accounts')
      },
      {
        path: 'account/:accountID',
        component: () => import('pages/account')
      },
      {
        path: 'account/',
        component: () => import('pages/account')
      },
      {
        path: 'categories',
        component: () => import('pages/categories')
      },
      {
        path: 'category/:categoryID',
        component: () => import('pages/category')
      },
      {
        path: 'category/',
        component: () => import('pages/category')
      },
      {
        path: 'transactions',
        component: () => import('pages/transactions')
      },
      {
        path: 'transfer/:accountID',
        component: () => import('pages/transfer')
      },
      {
        path: 'transaction/:accountID/:type',
        component: () => import('pages/newTransaction')
      },
      {
        path: 'transaction/:transactionID/',
        component: () => import('pages/editTransaction')
      },
      {
        path: 'choseAccount/:type',
        component: () => import('pages/choseAccount')
      },
      {
        path: 'settings',
        component: () => import('pages/settings')
      },
      {
        path: 'settings/restoreAccounts',
        component: () => import('pages/restoreAccounts')
      }
    ]
  },
  {
    path: '/auth',
    component: () => import('layouts/auth'),
    children: [
      {
        path: 'login',
        component: () => import('pages/login')
      },
      {
        path: 'register',
        component: () => import('pages/register')
      }
    ]
  },
  { // Always leave this as last one
    path: '*',
    component: () => import('pages/404')
  }
]
