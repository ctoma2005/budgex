export const setData = (state, payload) => {
  state.accounts = payload.raw.map(d => {
    var data = d.data()

    return {
      ID: d.id,
      Name: data.Name,
      Type: data.Type,
      Balance: data.Balance,
      Currency: payload.currencyFromRaw(data.Currency),
      Disabled: data.hasOwnProperty('Disabled') ? data.Disabled : false
    }
  })
}

export const accountAdded = (state, payload) => {
  state.accounts.push(payload)
}

export const accountDisabledChanged = (state, payload) => {
  var disabledDoc = state.accounts.filter(el => el.ID === payload.ID)[0]
  disabledDoc.Disabled = payload.Value
}

export const accountEdited = (state, payload) => {
  var editedDoc = state.accounts.filter(el => el.ID === payload.ID)[0]
  var idx = state.accounts.indexOf(editedDoc)

  var newDoc = {...editedDoc, ...payload.Props}
  state.accounts.splice(idx, 1, newDoc)
}
