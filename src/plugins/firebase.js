import * as firebase from 'firebase'
// eslint-disable-next-line no-unused-vars
import * as firestore from 'firebase/firestore'

import Vuelidate from 'vuelidate'

export default ({ Vue }) => {
  var config = {
    apiKey: 'AIzaSyCkISa6KH73yAE6AWW-HaIpQ_725ZJ13cg',
    authDomain: 'budgex-e481b.firebaseapp.com',
    databaseURL: 'https://budgex-e481b.firebaseio.com',
    projectId: 'budgex-e481b',
    storageBucket: 'budgex-e481b.appspot.com',
    messagingSenderId: '266616383270'
  }

  firebase.initializeApp(config)
  Vue.prototype.$fb = firebase

  Vue.use(Vuelidate)
}
