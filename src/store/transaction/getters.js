import {filterTypes} from '../global/state'
import {filterKeys} from './state'

export const transactionsEmpty = state => {
  return state.Transactions.length === 0
}

export const transactionCategories = (state, getters, rootState, rootGetters) => type => {
  if (!type) { return [] }

  var res = rootGetters[`category/sorted${type}`]
    .map(c => {
      return {
        label: c.Name,
        value: c.ID
      }
    })

  return res
}

export const optionsFilterCategories = (state, getters, rootState, rootGetters) => {
  var expenseFilter = state.Filters.filter(f => f.Key === filterKeys.TYPE)[0]

  if (!expenseFilter) return []

  var expenseGetter = 'category/sortedExpense'
  var incomeGetter = 'category/sortedIncome'

  if (expenseFilter.Value === null) { // all
    return rootGetters[expenseGetter]
      .map(c => {
        return {
          label: `${c.Name} (Expense)`,
          value: c.ID
        }
      })
      .concat(rootGetters[incomeGetter]
        .map(c => {
          return {
            label: `${c.Name} (Income)`,
            value: c.ID
          }
        }))
  }

  var getter = expenseFilter.Value ? expenseGetter : incomeGetter

  return rootGetters[getter]
    .map(c => {
      return {
        label: c.Name,
        value: c.ID
      }
    })
}

export const hasCategory = (state, getters, rootState, rootGetters) => transaction => {
  return rootState.category.categories.length > 0 && transaction.CategoryID
}

export const transactionCategory = (state, getters, rootState, rootGetters) => transaction => {
  var res = rootGetters['category/categoryWithID'](transaction.CategoryID)

  return res
}

export const transactionWithID = (state) => ID => {
  return state.Transactions.filter(a => a.ID === ID)[0]
}

export const filterByKey = (state) => key => {
  return state.Filters.filter(f => f.Key === key)[0]
}

export const filterValue = (state, getters) => key => {
  var filter = getters.filterByKey(key)

  return filter.Type === filterTypes.DATE ? filter.Value.Range : filter.Value
}
