export const load = async function (store, {fb}) {
  var db = fb.firestore()
  var uid = store.rootGetters['auth/uid']
  var cateogoriesSnapshot = await db.collection('portfolios').doc(uid).collection('categories').get()
  store.commit('setData', {raw: cateogoriesSnapshot.docs})
}

export const submitNewCategory = async function (store, payload) {
  var db = payload.fb.firestore()
  var uid = store.rootGetters['auth/uid']

  var newDoc = {
    Name: payload.Name,
    Type: payload.Type,
    Tags: payload.Tags
  }

  var ref = await db
    .collection('portfolios')
    .doc(uid)
    .collection('categories')
    .add(newDoc)

  newDoc.ID = ref.id

  store.commit('categoryAdded', newDoc)
}

export const editCategory = async function (store, payload) {
  var db = payload.fb.firestore()
  var uid = store.rootGetters['auth/uid']

  var editedDoc = {
    ID: payload.ID,
    Name: payload.Name,
    Type: payload.Type,
    Tags: payload.Tags
  }

  await db
    .collection('portfolios')
    .doc(uid)
    .collection('categories')
    .doc(editedDoc.ID)
    .set({
      Name: editedDoc.Name,
      Type: editedDoc.Type,
      Tags: editedDoc.Tags
    }, {merge: true})

  store.commit('categoryEdited', editedDoc)
}

export const deleteCategory = async function (store, payload) {
  var db = payload.fb.firestore()
  var uid = store.rootGetters['auth/uid']

  await db
    .collection('portfolios')
    .doc(uid)
    .collection('categories')
    .doc(payload.ID)
    .delete()

  store.commit('categoryDeleted', payload.ID)
}
