export const prime = async function (store, vue) {
  if (store.state.primed) return

  store.commit('setLoading', true)

  await store.dispatch('doAction', {
    route: 'account/load',
    args: {fb: vue.$fb},
    showLoading: false,
    errorMessage: 'Failed to load accounts'
  })

  await store.dispatch('doAction', {
    route: 'category/load',
    args: {fb: vue.$fb},
    showLoading: false,
    errorMessage: 'Failed to load categories'
  })

  store.commit('setPrimed', true)
  store.commit('setLoading', false)
}

export const doAction = async function (store, {route, args, showLoading, errorMessage}) {
  store.commit('clearError')
  store.commit('clearSuccess')
  if (showLoading) { store.commit('setLoading', true) }

  try {
    var res = await store.dispatch(route, args, {root: true})

    return {success: true, result: res}
  } catch (e) {
    console.log(e)
    store.commit('setError', typeof errorMessage === 'function' ? errorMessage(e) : errorMessage)
    return {success: false, result: e}
  } finally {
    if (showLoading) { store.commit('setLoading', false) }
  }
}
