import Vue from 'vue'
import Vuex from 'vuex'

import auth from './auth'
import account from './account'
import category from './category'
import global from './global'
import transaction from './transaction'

Vue.use(Vuex)
var store = new Vuex.Store({
  modules: {
    global,
    auth,
    account,
    category,
    transaction
  }
})

if (process.env.DEV && module.hot) {
  module.hot.accept(['./auth', './account', './category', './transaction'], () => {
    const authModule = require('./auth').default
    const accountModule = require('./account').default
    const categoryModule = require('./category').default
    const transactionModule = require('./transaction').default
    const globalModule = require('./global').default

    store.hotUpdate({ modules: {
      global: globalModule,
      auth: authModule,
      account: accountModule,
      category: categoryModule,
      transaction: transactionModule
    } })
  })
}

export default store
