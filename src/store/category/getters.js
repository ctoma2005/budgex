export const categoriesEmpty = (state) => {
  return state.categories.length === 0
}

export const expenseEmpty = (state, getters) => {
  return getters.sortedExpense.length === 0
}

export const incomeEmpty = (state, getters) => {
  return getters.sortedIncome.length === 0
}

export const sortedCategories = (state) => {
  return state.categories.sort(function (a, b) {
    if (a.Type > b.Type) {
      return 1
    } else if (a.Type < b.Type) {
      return -1
    } else return 0
  })
}

export const sortedExpense = (state, getters) => {
  return getters.sortedCategories.filter(c => c.Type === 'Expense')
}

export const sortedIncome = (state, getters) => {
  return getters.sortedCategories.filter(c => c.Type === 'Income')
}

export const tagsCountDescription = (state) => input => {
  if (!input.Tags || input.Tags.length === 0) { return 'No' }

  return input.Tags.length
}

export const tags = (state) => categoryID => {
  return state.categories.filter(c => c.ID === categoryID)[0].Tags.map(t => {
    return {
      label: t,
      value: t
    }
  })
}

export const categoryWithID = (state) => ID => {
  return state.categories.filter(a => a.ID === ID)[0]
}
