
export const authenticated = state => {
  return state.user !== null
}

export const uid = state => {
  return state.user === null ? null : state.user.uid
}
