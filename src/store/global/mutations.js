
export const setLoading = (state, payload) => {
  state.loading = payload
}

export const clearError = (state) => {
  state.error = null
}

export const setError = (state, payload) => {
  state.error = payload

  if (payload) { state.loading = false }
}

export const setSuccess = (state, payload) => {
  state.success = payload
}

export const clearSuccess = (state) => {
  state.success = null
}

export const setPrimed = (state, payload) => {
  state.primed = payload
}

export const setSlidingAnimation = (state, payload) => {
  state.slidingAnimation = payload
}
