export const setData = (state, payload) => {
  state.categories = payload.raw.map(d => {
    var data = d.data()

    return {
      ID: d.id,
      Name: data.Name,
      Type: data.Type,
      Tags: data.Tags
    }
  })
}

export const categoryAdded = (state, payload) => {
  state.categories.push(payload)
}

export const categoryDeleted = (state, ID) => {
  state.categories = state.categories.filter(el => el.ID !== ID)
}

export const categoryEdited = (state, doc) => {
  var editedDoc = state.categories.filter(el => el.ID === doc.ID)[0]

  editedDoc.Name = doc.Name
  editedDoc.Type = doc.Type
  editedDoc.Tags = doc.Tags
}
