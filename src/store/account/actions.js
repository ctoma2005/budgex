export const load = async function (store, {fb}) {
  var db = fb.firestore()
  var uid = store.rootGetters['auth/uid']
  var accountsSnapshot = await db.collection('portfolios').doc(uid).collection('accounts').get()
  store.commit('setData', {raw: accountsSnapshot.docs, currencyFromRaw: store.rootGetters['global/currencyFromRaw'](store.state)})
}

export const submitNewAccount = async function (store, payload) {
  var db = payload.fb.firestore()
  var uid = store.rootGetters['auth/uid']

  var ref = await db
    .collection('portfolios')
    .doc(uid)
    .collection('accounts')
    .add(payload.Props)

  payload.Props.ID = ref.id

  if (payload.Props.Currency) { payload.Props.Currency = store.rootGetters['global/currencyFromRaw'](store.state)(payload.Props.Currency) }

  store.commit('accountAdded', payload.Props)
}

export const editAccount = async function (store, payload) {
  var db = payload.fb.firestore()
  var uid = store.rootGetters['auth/uid']

  await db
    .collection('portfolios')
    .doc(uid)
    .collection('accounts')
    .doc(payload.ID)
    .set(payload.Props, {merge: true})

  if (payload.Props.Currency) {
    payload.Props.Currency = store.rootGetters['global/currencyFromRaw'](store.state)(payload.Props.Currency)
  }

  store.commit('accountEdited', {ID: payload.ID, Props: payload.Props})
}

export const disableAccount = async function (store, payload) {
  var db = payload.fb.firestore()
  var uid = store.rootGetters['auth/uid']

  await db
    .collection('portfolios')
    .doc(uid)
    .collection('accounts')
    .doc(payload.ID)
    .update({Disabled: true})

  store.commit('accountDisabledChanged', {ID: payload.ID, Value: true})
}

export const enableAccount = async function (store, payload) {
  var db = payload.fb.firestore()
  var uid = store.rootGetters['auth/uid']
  await db
    .collection('portfolios')
    .doc(uid)
    .collection('accounts')
    .doc(payload.ID)
    .update({Disabled: false})

  store.commit('accountDisabledChanged', {ID: payload.ID, Value: false})
  store.commit('global/setSuccess', `Account ${store.getters.accountWithID(payload.ID).Name} restored!`, {root: true})
}
