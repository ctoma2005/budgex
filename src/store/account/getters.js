export const accountsEmpty = (state, getters) => {
  return getters.activeAccounts.length === 0
}

export const activeAccounts = (state) => {
  return state.accounts.filter(a => !a.Disabled)
}

export const disabledAccounts = (state) => {
  return state.accounts.filter(a => a.Disabled)
}

export const accountsCount = (state, getters) => {
  return getters.activeAccounts.length
}

export const sortedAccounts = (state, getters) => {
  return getters.activeAccounts.sort(function (a, b) {
    if (a.Type > b.Type) {
      return 1
    } else if (a.Type < b.Type) {
      return -1
    }

    if (a.Balance > b.Balance) {
      return -1
    } else if (a.Balance < b.Balance) {
      return 1
    } else return 0
  })
}

export const tagsCount = (state) => input => {
  if (!input.Tags) { return 0 }

  return input.Tags.length
}

export const destinationAccounts = (state, getters, rootState) => sourceAccountID => {
  return (sourceAccountID
    ? getters.optionsAccounts.filter(a => a.value !== sourceAccountID)
    : getters.optionsAccounts)
}

export const optionsAccounts = (state, getters) => {
  return getters.sortedAccounts.map(acc => {
    return {
      label: acc.Name,
      value: acc.ID
    }
  })
}

export const accountWithID = (state) => ID => {
  return state.accounts.filter(a => a.ID === ID)[0]
}

export const acronym = (state) => acc => {
  return acc.Name.match(/\b(\w+)\b/g)
    .map(s => s[0].toUpperCase())
    .join('')
}
